package se.cambio.cds.demo.sampleapi.demo.controller;


import static org.hamcrest.Matchers.containsString;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.web.header.Header;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import se.cambio.cds.demo.sampleapi.demo.config.secutiry.AuthFilter;
import se.cambio.cds.demo.sampleapi.demo.model.User;
import se.cambio.cds.demo.sampleapi.demo.service.UserService;
import se.cambio.cds.demo.sampleapi.demo.token.TokenHandler;

import java.util.ArrayList;

import static org.hamcrest.Matchers.hasSize;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest
{
    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService service;

    @MockBean
    private TokenHandler tokenHandler;

    @InjectMocks
    private UserController userController;

    @Before
    public void setUp()
    {
        mvc = MockMvcBuilders.standaloneSetup(userController)
                .setControllerAdvice()
                .addFilters(new AuthFilter(tokenHandler))
                .build();
    }

    @Test
    @WithMockUser
    public void callEmployeeWithValidToken()
    {
        when(service.getUsers()).thenReturn(
                new ArrayList<User>()
        );

        when(tokenHandler.isValidToken("ABCD")).thenReturn(true);
        try {
            mvc.perform(get("/userList").header("token","ABCD")
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @WithMockUser
    public void callEmployeeWithInValidToken()
    {
        when(service.getUsers()).thenReturn(
                new ArrayList<User>()
        );

        when(tokenHandler.isValidToken("ABCD")).thenReturn(false);
        try {
            mvc.perform(get("/userList")
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isForbidden());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
