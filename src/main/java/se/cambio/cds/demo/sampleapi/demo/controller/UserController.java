package se.cambio.cds.demo.sampleapi.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.cambio.cds.demo.sampleapi.demo.model.User;
import se.cambio.cds.demo.sampleapi.demo.service.UserService;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value="/userList")
    public List<User> getUsers()
    {
        return userService.getUsers();
    }
}
