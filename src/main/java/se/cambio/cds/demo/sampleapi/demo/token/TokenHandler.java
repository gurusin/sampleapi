package se.cambio.cds.demo.sampleapi.demo.token;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Handles the methods of token creation and token handler
 *
 * @Author : Sudarshana Gurusinghe
 */

@Component
public class TokenHandler {
    private static final String TOKEN_REQUEST_URL = "https://idp.cambiocds.com/auth/realms/cds-platform/protocol/openid-connect/token";
    private static final String TOKEN_VALIDATE_URL = "https://idp.cambiocds.com/auth/realms/cds-platform/protocol/openid-connect/token/introspect";

    public String retrieveToken() throws Exception {

        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(TOKEN_REQUEST_URL);
        List<NameValuePair> arguments = getNameValuePairs();
        post.setEntity(new UrlEncodedFormEntity(arguments));
        HttpResponse response = client.execute(post);
        final String token = EntityUtils.toString(response.getEntity());
        return parseToken(token);

    }

    public boolean isValidToken(final String token) throws AuthenticationException {
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(TOKEN_VALIDATE_URL);
            List<NameValuePair> arguments = getParametersForTokenValidation(token);
            post.setEntity(new UrlEncodedFormEntity(arguments));
            HttpResponse response = client.execute(post);
            final TokenValidateResponse result = parseResponse(EntityUtils.toString(response.getEntity()));
            return result.isActive();
        } catch (IOException e) {
            throw new BadCredentialsException(e.getMessage());
        } catch (Exception e) {
            throw new BadCredentialsException(e.getMessage());
        }
    }

    private List<NameValuePair> getNameValuePairs() {
        List<NameValuePair> arguments = new ArrayList<>();
        arguments.add(new BasicNameValuePair("grant_type", "password"));
        arguments.add(new BasicNameValuePair("client_id", "gateway"));
        arguments.add(new BasicNameValuePair("username", "test"));
        arguments.add(new BasicNameValuePair("password", "wertygfds123"));
        arguments.add(new BasicNameValuePair("client_secret", "73a67ef3-677e-4ef4-931e-5cfd45daee01"));
        return arguments;
    }

    private List<NameValuePair> getParametersForTokenValidation(final String token) {
        List<NameValuePair> arguments = new ArrayList<>();
        arguments.add(new BasicNameValuePair("token", token));
        arguments.add(new BasicNameValuePair("client_id", "gateway"));
        arguments.add(new BasicNameValuePair("client_secret", "73a67ef3-677e-4ef4-931e-5cfd45daee01"));
        return arguments;
    }

    private Map<String, String> getParameters() {
        final Map<String, String> params = new HashMap<>();
        params.put("grant_type", "password");
        params.put("client_id", "gateway");
        params.put("username", "test");
        params.put("password", "wertygfds123");
        params.put("client_secret", "73a67ef3-677e-4ef4-931e-5cfd45daee01");
        return params;
    }

    public static void main(String[] args) {
        try {
            final TokenHandler handler = new TokenHandler();
//            final String token = handler.retrieveToken();
//            System.out.println(token);

            String token = "AAAAADDDDDddddeyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJpdWtOdUJkbzJZ" +
                    "QjFtdlRBVG8tcXBZOVMyRkxuREFwaGJhSXE2QkkxR29JIn0.eyJqdGkiOiJmOTJlODRkZi1j" +
                    "YjU5LTQ5NWMtOGI5ZC04YjRlZTcyNTViOTgiLCJleHAiOjE1MTY3ODEwMjAsIm5iZiI6MCwiaWF0I" +
                    "joxNTE2NzgwNzIwLCJpc3MiOiJodHRwczovL2lkcC5jYW1iaW9jZHMuY29tL2F1dGgvcmVhbG1zL2Nkcy1wbGF0Zm9ybSIsImF1ZCI6ImdhdGV3YXkiLCJzdWIiOiI2NTgxN2JkYy1mNWM3LTQ4MzMtYTA4OC0yZDdlY2JkYWQwZmYiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJnYXRld2F5IiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiNzRiNDZmMTEtZTExNi00YWY5LTgwNTEtYjQ3MzcwMTBjMzU4IiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInByZWZlcnJlZF91c2VybmFtZSI6InRlc3QifQ.HJUQtcAgtf-yJWzj9m591aM8jAhJFR72t6heJJ9EOmGhFk2WUUG0vmotPvHisriFvv3qBf4Diy1TvNYAqUPIham0-7wOsmbKp5paED8PP1gwKZUP4bcWsRc1XBwCPOJb7cfvEvsmtxROHY82dluvr7q_4CqT7ktWcERYwgTkLbs-JrcQEpN2sYvGUwMh7yMkuwgqdXFwnXrlBWawsgddB-PrkJ-cIejONZBKvTa5YTSPRq5T9ro8Mi1YBGP28Y4NvNfeWb3etNRPmkkbuY4HLWnmsiMd6b3M6R_qzBV9TcqcAyXzoL5BwPMkGa9ywzEHFE7dRCMJAM6uIce-nF8D6Q\n";
            // Valid Token
            handler.isValidToken(token);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String parseToken(final String token) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        AuthToken obj = mapper.readValue(token, AuthToken.class);
        return obj.getAccess_token();
    }

    private TokenValidateResponse parseResponse(final String result) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(result, TokenValidateResponse.class);
    }
}
