package se.cambio.cds.demo.sampleapi.demo.service;

import org.springframework.stereotype.Service;
import se.cambio.cds.demo.sampleapi.demo.model.User;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    public List<User> getUsers()
    {
        final List<User> users = new ArrayList<>();
        users.add(createUser("Sudarshana","Gurusinghe"));
        users.add(createUser("John","Adams"));
        return users;
    }

    private User createUser(final String firstName, final String lastName)
    {
        final User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }
}
