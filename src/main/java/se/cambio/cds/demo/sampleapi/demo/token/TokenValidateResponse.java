package se.cambio.cds.demo.sampleapi.demo.token;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Represents the response for the token validation
 *
 * @author: Sudarshana Gurusinghe
 * @since Jan 2018

 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class TokenValidateResponse {

    private String jti;
    private long exp;
    private long iat;
    private String auth_time;
    private boolean active;

    public String getJti() {
        return jti;
    }

    public void setJti(String jti) {
        this.jti = jti;
    }

    public long getExp() {
        return exp;
    }

    public void setExp(long exp) {
        this.exp = exp;
    }

    public long getIat() {
        return iat;
    }

    public void setIat(long iat) {
        this.iat = iat;
    }

    public String getAuth_time() {
        return auth_time;
    }

    public void setAuth_time(String auth_time) {
        this.auth_time = auth_time;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
