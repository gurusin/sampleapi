package se.cambio.cds.demo.sampleapi.demo.config.secutiry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.filter.GenericFilterBean;
import se.cambio.cds.demo.sampleapi.demo.token.TokenHandler;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthFilter extends GenericFilterBean
{

    private TokenHandler tokenHandler;

    @Autowired
    public AuthFilter(final TokenHandler tokenHandler) {
        this.tokenHandler = tokenHandler;
    }

    @Override

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {

        try {
            HttpServletRequest request = (HttpServletRequest)servletRequest;
            String token = request.getHeader("token");
            if (tokenHandler.isValidToken(token))
            {
                filterChain.doFilter(servletRequest,servletResponse);
            }else
            {
                HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
                httpResponse.setContentType("application/json");
                httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, "Unauthorized request. Please login");
                return;
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }


    }
}
