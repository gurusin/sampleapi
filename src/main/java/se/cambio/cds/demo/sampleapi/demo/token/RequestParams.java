package se.cambio.cds.demo.sampleapi.demo.token;

public class RequestParams
{
    private String grant_type = "password";
    private String client_id = "gateway";
    private String username ="test";
    private String password ="wertygfds123";
    private String client_secret ="73a67ef3-677e-4ef4-931e-5cfd45daee01";

    public String getGrant_type() {
        return grant_type;
    }

    public void setGrant_type(String grant_type) {
        this.grant_type = grant_type;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getClient_secret() {
        return client_secret;
    }

    public void setClient_secret(String client_secret) {
        this.client_secret = client_secret;
    }
}
